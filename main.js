window.onload = () => {
    new Swiper('.swiper-container', {
        loop: true,
        autoHeight: true,
        autoplay: {
            delay: 4000,
            disableOnInteraction: true
        },
        speed: 500,
        effect: 'fade',
    });
    const hum = document.getElementById('hamburger');
    const close = document.getElementById("close");
    const nav = document.getElementById('sp-nav');

    hum.onclick = () => {
        nav.classList.add('toggle');
    }
    close.onclick = () => {
        nav.classList.remove('toggle');
    }
}